package com.alepet.util;

public class MessageResponse {
	public final static String OK = "Ok";
	public final static String UPDATED = "Atualizado";
	public final static String DELETED = "Deletado";
	public final static String CREATED = "Criado";
	public final static String CONSTRAIN_ERROR_SPECIE = "Existem raças cadastradas com esta espécie";
	public static final String GENERIC_ERROR = "Algo inesperado ocorreu";
}
  