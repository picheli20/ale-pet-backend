package com.alepet.util;

import javax.servlet.http.HttpServletResponse;

import org.springframework.data.repository.CrudRepository;

public class GenericRequest<T> {
	public T type;

	public GenericResponse<Iterable<T>> get(CrudRepository genericCrud){
		GenericResponse<Iterable<T>> response = new GenericResponse<>();
		response.setData(genericCrud.findAll());
		response.setMessage(MessageResponse.OK);
		return response;
	}
	
	public GenericResponse<T> get(CrudRepository genericCrud, T data){
		GenericResponse<T> response = new GenericResponse<>();
		response.setData(data);
		response.setMessage(MessageResponse.OK);
		return response;
		
	}
	
	public GenericResponse<T> put(CrudRepository genericCrud, T data, HttpServletResponse responseConf){
		GenericResponse<T> response = new GenericResponse<>();
		try{
			T saved = (T) genericCrud.save(data);		
			response.setData(saved);
			response.setMessage(MessageResponse.CREATED);
		}catch(Exception ex){
			response.setMessage(MessageResponse.GENERIC_ERROR);
			responseConf.setStatus(500);
		}
		return response;
	}
	
	public GenericResponse<T> post(CrudRepository genericCrud, T data){
		GenericResponse<T> response = new GenericResponse<>();
		try{
			T saved = (T) genericCrud.save(data);		
			response.setData(saved);
			response.setMessage(MessageResponse.UPDATED);
		}catch(Exception ex){
			response.setMessage(MessageResponse.GENERIC_ERROR);
		}
		return response;
	}
	
	public GenericResponse<T> delete(CrudRepository genericCrud, T data){
		GenericResponse<T> response = new GenericResponse<>();
		try{
			genericCrud.delete(data);		
			response.setData(data);
			response.setMessage(MessageResponse.DELETED);
		}catch(Exception ex){
			response.setMessage(MessageResponse.GENERIC_ERROR);
		}
		return response;
	}
}
