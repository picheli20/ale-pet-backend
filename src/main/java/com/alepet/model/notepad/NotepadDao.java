package com.alepet.model.notepad;

import org.springframework.data.repository.CrudRepository;

public interface NotepadDao extends CrudRepository<Notepad, Long>{
	public Notepad findById(Long id);
}
