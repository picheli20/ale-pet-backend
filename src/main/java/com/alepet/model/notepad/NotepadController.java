package com.alepet.model.notepad;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.alepet.util.GenericRequest;
import com.alepet.util.GenericResponse;

@RestController
@RequestMapping("/api/v1/notepad")
public class NotepadController {
	@Autowired
	private NotepadDao notepadDao;
	
	GenericRequest<Notepad> genericRequest = new GenericRequest<>();

	@RequestMapping(method=RequestMethod.GET, path="/")
	public GenericResponse<Iterable<Notepad>> getColors(){
		return genericRequest.get(notepadDao);
	}

	@RequestMapping(method=RequestMethod.PUT, path="/")
	public GenericResponse<Notepad> putColor(@RequestBody Notepad item, HttpServletResponse responseConf){
		return genericRequest.put(notepadDao, item, responseConf);
	}

	@RequestMapping(method=RequestMethod.POST, path="/")
	public GenericResponse<Notepad> postCondo(@RequestBody Notepad item){
		return genericRequest.post(notepadDao, item);
	}
	
	@RequestMapping(method=RequestMethod.GET, path="/{id}")
	public GenericResponse<Notepad> getOneColor(@PathVariable Long id){
		return genericRequest.get(notepadDao, notepadDao.findById(id));
	}
	
	@RequestMapping(method=RequestMethod.DELETE, path="/{id}")
	public GenericResponse<Notepad> deleteColor(@PathVariable Long id){
		Notepad item = notepadDao.findById(id);
		return genericRequest.delete(notepadDao, item);
	}
}
