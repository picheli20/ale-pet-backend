package com.alepet.model.attendance;

import org.springframework.data.repository.CrudRepository;

public interface AttendanceDao extends CrudRepository<Attendance, Long>{
	public Attendance findById(Long id);

}
