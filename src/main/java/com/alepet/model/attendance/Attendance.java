package com.alepet.model.attendance;

import java.sql.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.alepet.model.core.checklist.Checklist;
import com.alepet.model.core.pet.Pet;
import com.alepet.model.product.service.Service;
import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table
public class Attendance {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String observation;	
	private Date date;
	@ManyToMany
	private List<Checklist> checklist;
	@ManyToMany
	private List<Service> service;
	@JsonBackReference("PetAttendance")
	@ManyToOne
	private Pet pet;


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Pet getPet() {
		return pet;
	}

	public void setPet(Pet pet) {
		this.pet = pet;
	}

	public List<Service> getService() {
		return service;
	}

	public void setService(List<Service> service) {
		this.service = service;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getObservation() {
		return observation;
	}

	public void setObservation(String observation) {
		this.observation = observation;
	}

	public List<Checklist> getChecklist() {
		return checklist;
	}

	public void setChecklist(List<Checklist> checklist) {
		this.checklist = checklist;
	}
	
}
