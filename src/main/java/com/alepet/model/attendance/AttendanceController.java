package com.alepet.model.attendance;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.alepet.model.core.pet.Pet;
import com.alepet.model.core.pet.PetDao;
import com.alepet.util.GenericRequest;
import com.alepet.util.GenericResponse;
import com.alepet.util.MessageResponse;

@RestController
@RequestMapping("/api/v1/attendance")
public class AttendanceController {

	@Autowired
	private AttendanceDao attedanceDao;
	@Autowired
	private PetDao petDao;
	
	GenericRequest<Attendance> genericRequest = new GenericRequest<>();
	
	@RequestMapping(method = RequestMethod.GET, path = "/")
	public GenericResponse<Iterable<Attendance>> get() {
		GenericResponse<Iterable<Attendance>> response = new GenericResponse<>();
		response.setData(attedanceDao.findAll());
		response.setMessage(MessageResponse.OK);
		return response;
	}
	
	@RequestMapping(method = RequestMethod.PUT, path = "/{petId}")
	public GenericResponse<Attendance> put(@RequestBody Attendance attendance,@PathVariable Long petId, HttpServletResponse responseConf) {
		GenericResponse<Attendance> response = new GenericResponse<>();
		try {
			Pet pet = petDao.findById(petId);
			attendance.setPet(pet);
			Attendance createdAttendance = attedanceDao.save(attendance);
			List<Attendance> attendances = pet.getAttedance();
			attendances.add(createdAttendance);
			pet.setAttedance(attendances);
			petDao.save(pet);
			response.setData(createdAttendance);
			response.setMessage(MessageResponse.CREATED);
		} catch (Exception ex) {
			response.setMessage(MessageResponse.GENERIC_ERROR);
			responseConf.setStatus(500);
		}
		return response;
	}
	
	@RequestMapping(method = RequestMethod.GET, path = "/{attendanceId}")
	public GenericResponse<Pet> getOne(@PathVariable Long attendanceId, HttpServletResponse responseConf) {
		GenericResponse<Pet> response = new GenericResponse<>();
		try {
			Attendance att = attedanceDao.findById(attendanceId);
			Pet pet = att.getPet();
			// TODO fix this work around
			List<Attendance> attList = new ArrayList<>();
			attList.add(att);
			pet.setAttedance(attList);
			response.setData(pet);
			response.setMessage(MessageResponse.OK);
		} catch (Exception ex) {
			response.setMessage(MessageResponse.GENERIC_ERROR);
			responseConf.setStatus(500);
		}
		return response;
	}
	@RequestMapping(method=RequestMethod.DELETE, path="/{id}")
	public GenericResponse<Attendance> deleteColor(@PathVariable Long id){
		Attendance att = attedanceDao.findById(id);
		return genericRequest.delete(attedanceDao, att);
	}
}
