package com.alepet.model.core.breed;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.alepet.model.core.specie.Specie;
import com.alepet.model.core.specie.SpecieDao;
import com.alepet.util.GenericResponse;
import com.alepet.util.MessageResponse;

@RestController
@RequestMapping("/api/v1/breed")
public class BreedController {
	@Autowired
	private BreedDao breedDao;
	@Autowired
	private SpecieDao specieDao;

	@RequestMapping(method = RequestMethod.GET, path = "/")
	public GenericResponse<Iterable<Breed>> get() {
		GenericResponse<Iterable<Breed>> response = new GenericResponse<>();
		response.setData(breedDao.findAll());
		response.setMessage(MessageResponse.OK);
		return response;
	}

	@RequestMapping(method = RequestMethod.GET, path = "/{id}")
	public GenericResponse<Iterable<Breed>> getBySpecie(@PathVariable Long id) {
		GenericResponse<Iterable<Breed>> response = new GenericResponse<>();
		Specie specie = specieDao.findById(id);
		response.setData(breedDao.findBySpecie(specie));
		response.setMessage(MessageResponse.OK);
		return response;
	}

	@RequestMapping(method = RequestMethod.PUT, path = "/")
	public GenericResponse<Breed> put(@RequestBody Breed breed, HttpServletResponse responseConf) {
		GenericResponse<Breed> response = new GenericResponse<Breed>();
		try {
			response.setData(breedDao.save(breed));
			response.setMessage(MessageResponse.CREATED);
		} catch (Exception ex) {
			response.setMessage(MessageResponse.GENERIC_ERROR);
			responseConf.setStatus(500);
		}
		return response;
	}

	@RequestMapping(method = RequestMethod.DELETE, path = "/{id}")
	public GenericResponse<Breed> delete(@PathVariable Long id, HttpServletResponse responseConf) {
		GenericResponse<Breed> response = new GenericResponse<Breed>();
		Breed breed = breedDao.findById(id);
		try {
			breedDao.delete(breed);
			response.setData(breed);
			response.setMessage(MessageResponse.DELETED);
		} catch (Exception ex) {
			response.setMessage(MessageResponse.GENERIC_ERROR);
			responseConf.setStatus(500);
		}

		return response;
	}

	@RequestMapping(method = RequestMethod.POST, path = "/")
	public GenericResponse<Breed> save(@RequestBody Breed breed, HttpServletResponse responseConf) {
		GenericResponse<Breed> response = new GenericResponse<Breed>();
		try {
			response.setData(breedDao.save(breed));
			response.setMessage(MessageResponse.UPDATED);
		} catch (Exception ex) {
			response.setMessage(MessageResponse.GENERIC_ERROR);
			responseConf.setStatus(500);
		}
		return response;
	}
}
