package com.alepet.model.core.breed;

import javax.transaction.Transactional;

import org.springframework.data.repository.CrudRepository;

import com.alepet.model.core.specie.Specie;

@Transactional
public interface BreedDao extends CrudRepository<Breed, Long>{
	public Breed findById(Long id);
	public Iterable<Breed> findBySpecie(Specie specie);
	
}
