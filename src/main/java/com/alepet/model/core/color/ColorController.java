package com.alepet.model.core.color;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.alepet.util.GenericRequest;
import com.alepet.util.GenericResponse;

@RestController
@RequestMapping("/api/v1/color")
public class ColorController {
	@Autowired
	private ColorDao colorDao;
	
	GenericRequest<Color> genericRequest = new GenericRequest<>();

	@RequestMapping(method=RequestMethod.GET, path="/")
	public GenericResponse<Iterable<Color>> getColors(){
		return genericRequest.get(colorDao);
	}

	@RequestMapping(method=RequestMethod.PUT, path="/")
	public GenericResponse<Color> putColor(@RequestBody Color color, HttpServletResponse responseConf){
		return genericRequest.put(colorDao, color, responseConf);
	}

	@RequestMapping(method=RequestMethod.POST, path="/")
	public GenericResponse<Color> postCondo(@RequestBody Color color){
		return genericRequest.post(colorDao, color);
	}
	
	@RequestMapping(method=RequestMethod.GET, path="/{id}")
	public GenericResponse<Color> getOneColor(@PathVariable Long id){
		return genericRequest.get(colorDao, colorDao.findById(id));
	}
	
	@RequestMapping(method=RequestMethod.DELETE, path="/{id}")
	public GenericResponse<Color> deleteColor(@PathVariable Long id){
		Color color = colorDao.findById(id);
		return genericRequest.delete(colorDao, color);
	}
}
