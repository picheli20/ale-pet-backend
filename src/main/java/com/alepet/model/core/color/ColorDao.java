package com.alepet.model.core.color;

import javax.transaction.Transactional;

import org.springframework.data.repository.CrudRepository;

@Transactional
public interface ColorDao extends CrudRepository<Color, Long>{
	public Color findByName(String name);
	public Color findById(Long id);
}
