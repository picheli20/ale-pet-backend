package com.alepet.model.core.condo;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.alepet.util.GenericResponse;
import com.alepet.util.MessageResponse;

@RestController
@RequestMapping("/api/v1/condo")
public class CondoController {
	
	@Autowired
	private CondoDao condoDao;
	
	@RequestMapping(method=RequestMethod.GET, path="/")
	public GenericResponse<Iterable<Condo>> getCondo(){
		GenericResponse<Iterable<Condo>> response = new GenericResponse<>();
		response.setData(condoDao.findAll());
		response.setMessage(MessageResponse.OK);
		return response;
	}
	
	@RequestMapping(method=RequestMethod.PUT, path="/")
	public GenericResponse<Condo> putCondo(@RequestBody Condo condo, HttpServletResponse responseConf){
		GenericResponse<Condo> response = new GenericResponse<>();
		try{
			Condo saved = condoDao.save(condo);		
			response.setData(saved);
			response.setMessage(MessageResponse.CREATED);
		}catch(Exception ex){
			response.setMessage(MessageResponse.GENERIC_ERROR);
			responseConf.setStatus(500);
		}
		return response;
	}
	
	@RequestMapping(method=RequestMethod.POST, path="/")
	public GenericResponse<Condo> postCondo(@RequestBody Condo condo){
		GenericResponse<Condo> response = new GenericResponse<>();
		try{
			Condo saved = condoDao.save(condo);		
			response.setData(saved);
			response.setMessage(MessageResponse.UPDATED);
		}catch(Exception ex){
			response.setMessage(MessageResponse.GENERIC_ERROR);
		}
		return response;
	}
	
	@RequestMapping(method=RequestMethod.DELETE, path="/{id}")
	public GenericResponse<Condo> deleteCondo(@PathVariable Long id){
		GenericResponse<Condo> response = new GenericResponse<>();
		Condo condo = condoDao.findById(id);
		try{
			condoDao.delete(condo);		
			response.setData(condo);
			response.setMessage(MessageResponse.DELETED);
		}catch(Exception ex){
			response.setMessage(MessageResponse.GENERIC_ERROR);
		}
		return response;
	}

}
