package com.alepet.model.core.condo;

import javax.transaction.Transactional;

import org.springframework.data.repository.CrudRepository;

@Transactional
public interface CondoDao extends CrudRepository<Condo, Long> {
	public Condo findByName(String name);
	public Condo findById(Long id);
}
