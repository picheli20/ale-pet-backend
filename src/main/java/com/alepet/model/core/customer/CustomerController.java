package com.alepet.model.core.customer;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.alepet.model.core.condo.Condo;
import com.alepet.util.GenericResponse;
import com.alepet.util.MessageResponse;

@RestController
@RequestMapping("/api/v1/customer")
public class CustomerController {
	@Autowired
	private CustomerDao customerDao;
	
	@RequestMapping(method=RequestMethod.GET, path="/")
	public GenericResponse<Iterable<Customer>> getCustomer(){
		GenericResponse<Iterable<Customer>> response = new GenericResponse<>();
		response.setData(customerDao.findAll());
		response.setMessage(MessageResponse.OK);
		return response;
	}

	
	@RequestMapping(method = RequestMethod.PUT, path = "/")
	public GenericResponse<Customer> putCustomer(@RequestBody Customer customer, HttpServletResponse responseConf) {
		GenericResponse<Customer> response = new GenericResponse<Customer>();
		try {
			response.setData(customerDao.save(customer));
			response.setMessage(MessageResponse.CREATED);
		} catch (Exception ex) {
			response.setMessage(MessageResponse.GENERIC_ERROR);
			responseConf.setStatus(500);
		}
		return response;
	}
	
	@RequestMapping(method = RequestMethod.POST, path = "/")
	public GenericResponse<Customer> postCustomer(@RequestBody Customer customer, HttpServletResponse responseConf) {
		GenericResponse<Customer> response = new GenericResponse<Customer>();
		try {
			response.setData(customerDao.save(customer));
			response.setMessage(MessageResponse.CREATED);
		} catch (Exception ex) {
			response.setMessage(MessageResponse.GENERIC_ERROR);
			responseConf.setStatus(500);
		}
		return response;
	}
	
	@RequestMapping(method=RequestMethod.DELETE, path="/{id}")
	public GenericResponse<Customer> deleteCustomer(@PathVariable Long id){
		GenericResponse<Customer> response = new GenericResponse<>();
		Customer customer = customerDao.findById(id);
		try{
			customerDao.delete(customer);		
			response.setData(customer);
			response.setMessage(MessageResponse.DELETED);
		}catch(Exception ex){
			response.setMessage(MessageResponse.GENERIC_ERROR);
		}
		return response;
	}
}
