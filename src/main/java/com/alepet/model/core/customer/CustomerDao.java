package com.alepet.model.core.customer;

import javax.transaction.Transactional;

import org.springframework.data.repository.CrudRepository;

@Transactional
public interface CustomerDao extends CrudRepository<Customer, Long> {
	public Customer findByName(String name);
	public Customer findById(Long id);

}
