package com.alepet.model.core.specie;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.alepet.model.core.breed.Breed;
import com.alepet.model.core.breed.BreedDao;
import com.alepet.util.GenericResponse;
import com.alepet.util.MessageResponse;

@RestController
@RequestMapping("/api/v1/specie")
public class SpecieController {
	@Autowired
	private SpecieDao specieDao;
	@Autowired
	private BreedDao breedDao;

	@RequestMapping(method = RequestMethod.GET, path = "/")
	public GenericResponse<Iterable<Specie>> getSpecies(HttpServletResponse responseConf) {
		GenericResponse<Iterable<Specie>> response = new GenericResponse<Iterable<Specie>>();
		response.setData(specieDao.findAll());
		response.setMessage(MessageResponse.OK);
		return response;
	}

	@RequestMapping(method = RequestMethod.PUT, path = "/")
	public GenericResponse<Specie> putSpecies(@RequestBody Specie specie, HttpServletResponse responseConf) {
		GenericResponse<Specie> response = new GenericResponse<Specie>();
		try {
			response.setData(specieDao.save(specie));
			response.setMessage(MessageResponse.CREATED);
		} catch (Exception ex) {
			response.setMessage(MessageResponse.GENERIC_ERROR);
			responseConf.setStatus(500);
		}
		return response;
	}

	@RequestMapping(method = RequestMethod.DELETE, path = "/{id}")
	public GenericResponse<Specie> deleteSpecies(@PathVariable Long id, HttpServletResponse responseConf) {
		GenericResponse<Specie> response = new GenericResponse<Specie>();

		Specie specie = specieDao.findById(id);
		Iterable<Breed> breedList = breedDao.findBySpecie(specie);

		if (breedList.iterator().hasNext()) {
			response.setMessage(MessageResponse.CONSTRAIN_ERROR_SPECIE);
			responseConf.setStatus(409);
		} else {
			specieDao.delete(specie);
			response.setMessage(MessageResponse.DELETED);
		}
		return response;
	}

	@RequestMapping(method = RequestMethod.POST, path = "/")
	public GenericResponse<Specie> updateSpecies(@RequestBody Specie specie, HttpServletResponse responseConf) {
		GenericResponse<Specie> response = new GenericResponse<Specie>();
		try {
			response.setData(specieDao.save(specie));
			response.setMessage(MessageResponse.UPDATED);
		} catch (Exception ex) {
			response.setMessage(MessageResponse.GENERIC_ERROR);
			responseConf.setStatus(500);
		}
		return response;
	}
}
