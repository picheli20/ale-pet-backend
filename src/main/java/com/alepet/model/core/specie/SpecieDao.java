package com.alepet.model.core.specie;

import javax.transaction.Transactional;

import org.springframework.data.repository.CrudRepository;

@Transactional
public interface SpecieDao extends CrudRepository<Specie, Long> {
	public Specie findById(Long id);
}
