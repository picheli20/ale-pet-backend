package com.alepet.model.core.checklist;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.alepet.util.GenericResponse;
import com.alepet.util.MessageResponse;

@RestController
@RequestMapping("/api/v1/checklist")
public class ChecklistController {

	@Autowired
	private ChecklistDao checklistDao;
	
	@RequestMapping(method = RequestMethod.GET, path = "/")
	public GenericResponse<Iterable<Checklist>> get() {
		GenericResponse<Iterable<Checklist>> response = new GenericResponse<>();
		response.setData(checklistDao.findAll());
		response.setMessage(MessageResponse.OK);
		return response;
	}
}
