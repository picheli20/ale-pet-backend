package com.alepet.model.core.checklist;

import org.springframework.data.repository.CrudRepository;

public interface ChecklistDao extends CrudRepository<Checklist, Long>{

}
