package com.alepet.model.core.pet;


import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.alepet.model.attendance.Attendance;
import com.alepet.model.attendance.AttendanceDao;
import com.alepet.util.GenericResponse;
import com.alepet.util.MessageResponse;

@RestController
@RequestMapping("/api/v1/pet")
public class PetController {
	@Autowired
	private PetDao petDao;
	@Autowired
	private AttendanceDao attedanceDao;
	
	@RequestMapping(method=RequestMethod.GET, path="/")
	public GenericResponse<Iterable<Pet>> getPets(){
		GenericResponse<Iterable<Pet>> response = new GenericResponse<>();
		response.setData(petDao.findAll());
		response.setMessage(MessageResponse.OK);
		return response;
	}

	@RequestMapping(method = RequestMethod.PUT, path = "/")
	public GenericResponse<Pet> putPet(@RequestBody Pet pet, HttpServletResponse responseConf) {
		GenericResponse<Pet> response = new GenericResponse<>();
		try {
			response.setData(petDao.save(pet));
			response.setMessage(MessageResponse.CREATED);
		} catch (Exception ex) {
			response.setMessage(MessageResponse.GENERIC_ERROR);
			responseConf.setStatus(500);
		}
		return response;
	}
	
	@RequestMapping(method=RequestMethod.GET, path="/{id}")
	public GenericResponse<Pet> getPet(@PathVariable Long id){
		GenericResponse<Pet> response = new GenericResponse<>();
		response.setData(petDao.findById(id));
		response.setMessage(MessageResponse.OK);
		return response;
	}
	

	
	@RequestMapping(method=RequestMethod.POST, path="/")
	public GenericResponse<Pet> postPet(@RequestBody Pet pet){
		GenericResponse<Pet> response = new GenericResponse<>();
		try{
			Pet saved = petDao.save(pet);		
			response.setData(saved);
			response.setMessage(MessageResponse.UPDATED);
		}catch(Exception ex){
			response.setMessage(MessageResponse.GENERIC_ERROR);
		}
		return response;
	}
	
	@RequestMapping(method=RequestMethod.DELETE, path="/{id}")
	public GenericResponse<Pet> deletePet(@PathVariable Long id){
		GenericResponse<Pet> response = new GenericResponse<>();
		Pet pet = petDao.findById(id);
		
		for(int i = 0; i < pet.getAttedance().size(); i++){
			Attendance att = pet.getAttedance().get(i);
			attedanceDao.delete(att);
		}
		
		try{
			petDao.delete(pet);
			response.setData(null);
			response.setMessage(MessageResponse.DELETED);
		}catch(Exception ex){
			response.setMessage(MessageResponse.GENERIC_ERROR);
		}
		return response;
	}
	

}
