package com.alepet.model.core.pet;

import javax.transaction.Transactional;
import org.springframework.data.repository.CrudRepository;

@Transactional
public interface PetDao extends CrudRepository<Pet, Long> {
	public Pet findByName(String name);
	public Pet findById(Long id);
}
