package com.alepet.model.core.pet;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.alepet.model.attendance.Attendance;
import com.alepet.model.core.breed.Breed;
import com.alepet.model.core.color.Color;
import com.alepet.model.core.customer.Customer;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table
public class Pet {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@NotNull
	private String name;
	
	@NotNull
	private char genre;
	
	@NotNull
	private Date born;
	private Date vaccine;
	private String observation;
	private int pelage;
	private int size;
	
	@ManyToMany
	private List<Color> color;
	
	@JsonManagedReference("PetAttendance")
	@OneToMany
	private List<Attendance> attendance;

	@ManyToOne
	private Breed breed;

	@ManyToOne
	private Customer customer;

	
	public int getPelage() {
		return pelage;
	}
	public void setPelage(int pelage) {
		this.pelage = pelage;
	}
	public int getSize() {
		return size;
	}
	public void setSize(int size) {
		this.size = size;
	}
	public List<Attendance> getAttendance() {
		return attendance;
	}
	public void setAttendance(List<Attendance> attendance) {
		this.attendance = attendance;
	}
	public List<Attendance> getAttedance() {
		return attendance;
	}
	public void setAttedance(List<Attendance> attedance) {
		this.attendance = attedance;
	}
	public Customer getCustomer() {
		return customer;
	}
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	public Breed getBreed() {
		return breed;
	}
	public void setBreed(Breed breed) {
		this.breed = breed;
	}
	public Pet(String name, char genre, Date born) {
		this.name = name;
		this.genre = genre;
		this.born = born;
	}
	public Pet() {}
	
	public List<Color> getColor() {
		return color;
	}
	public void setColor(List<Color> color) {
		this.color = color;
	}

	public Date getVaccine() {
		return vaccine;
	}

	public void setVaccine(Date vaccine) {
		this.vaccine = vaccine;
	}

	public String getObservation() {
		return observation;
	}

	public void setObservation(String observation) {
		this.observation = observation;
	}

	public Date getBorn() {
		return born;
	}

	public void setBorn(Date born) {
		this.born = born;
	}

	public char getGenre() {
		return genre;
	}

	public void setGenre(char genre) {
		this.genre = genre;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
}
