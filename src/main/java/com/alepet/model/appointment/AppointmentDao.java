package com.alepet.model.appointment;

import javax.transaction.Transactional;

import org.springframework.data.repository.CrudRepository;

@Transactional
public interface AppointmentDao extends CrudRepository<Appointment, Long>{
	public Appointment findById(Long id);
}
