package com.alepet.model.appointment;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.alepet.model.core.color.Color;
import com.alepet.util.GenericRequest;
import com.alepet.util.GenericResponse;

@RestController
@RequestMapping("/api/v1/appointment")
public class AppointmentController {
	@Autowired
	private AppointmentDao appointmentDao;
	
	GenericRequest<Appointment> genericRequest = new GenericRequest<>();
	
	@RequestMapping(method=RequestMethod.GET, path="/")
	public GenericResponse<Iterable<Appointment>> getColors(){
		return genericRequest.get(appointmentDao);
	}

	@RequestMapping(method=RequestMethod.PUT, path="/")
	public GenericResponse<Appointment> putColor(@RequestBody Appointment app, HttpServletResponse responseConf){
		return genericRequest.put(appointmentDao, app, responseConf);
	}

	@RequestMapping(method=RequestMethod.POST, path="/")
	public GenericResponse<Appointment> postCondo(@RequestBody Appointment app){
		return genericRequest.post(appointmentDao, app);
	}
	
	@RequestMapping(method=RequestMethod.GET, path="/{id}")
	public GenericResponse<Appointment> getOneColor(@PathVariable Long id){
		return genericRequest.get(appointmentDao, appointmentDao.findById(id));
	}
	
	@RequestMapping(method=RequestMethod.DELETE, path="/{id}")
	public GenericResponse<Appointment> deleteColor(@PathVariable Long id){
		Appointment app = appointmentDao.findById(id);
		return genericRequest.delete(appointmentDao, app);
	}
}
