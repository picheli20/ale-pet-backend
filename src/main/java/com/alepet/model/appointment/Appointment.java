package com.alepet.model.appointment;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.alepet.model.core.pet.Pet;
import com.alepet.model.product.service.Service;

@Entity
@Table
public class Appointment {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private Long duration; // in minutes
	private Long stackID; // stack of creation
	private String observation;	
	@ManyToOne
	private Service type;
	private Long date;
	@ManyToOne
	private Pet pet;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getDuration() {
		return duration;
	}
	public void setDuration(Long duration) {
		this.duration = duration;
	}
	public String getObservation() {
		return observation;
	}
	public void setObservation(String observation) {
		this.observation = observation;
	}
	public Service getType() {
		return type;
	}
	public void setType(Service type) {
		this.type = type;
	}
	public Long getDate() {
		return date;
	}
	public void setDate(Long date) {
		this.date = date;
	}
	public Pet getPet() {
		return pet;
	}
	public void setPet(Pet pet) {
		this.pet = pet;
	}
	public Long getStackID() {
		return stackID;
	}
	public void setStackID(Long stackID) {
		this.stackID = stackID;
	}
}
