package com.alepet.model.product.category;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.alepet.util.GenericResponse;
import com.alepet.util.MessageResponse;

@RestController
@RequestMapping("api/v1/category")
public class CategoryController{
	
	@Autowired
	private CategoryDao categoryDao;
	
	@RequestMapping(path="/", method=RequestMethod.GET)
	public GenericResponse<Iterable<Category>> getCategory(){
		GenericResponse<Iterable<Category>> response = new GenericResponse<>();
		response.setData(categoryDao.findAll());
		response.setMessage(MessageResponse.OK);
		return response;
	}
	
	@RequestMapping(path="/", method=RequestMethod.PUT)
	public GenericResponse<Category> putCategory(@RequestBody Category category, HttpServletResponse responseConf){
		GenericResponse<Category> response = new GenericResponse<>();
		try {
			response.setData(categoryDao.save(category));
			response.setMessage(MessageResponse.CREATED);
		} catch (Exception ex) {
			response.setMessage(MessageResponse.GENERIC_ERROR);
			responseConf.setStatus(500);
		}

		return response;
	}
	
	@RequestMapping(path="/", method=RequestMethod.POST)
	public GenericResponse<Category> postCategory(@RequestBody Category category, HttpServletResponse responseConf){
		GenericResponse<Category> response = new GenericResponse<>();
		try {
			response.setData(categoryDao.save(category));
			response.setMessage(MessageResponse.UPDATED);
		} catch (Exception ex) {
			response.setMessage(MessageResponse.GENERIC_ERROR);
			responseConf.setStatus(500);
		}

		return response;
	}
	
	@RequestMapping(path="/", method=RequestMethod.DELETE)
	public GenericResponse<Category> deleteCategory(@RequestBody Category category, HttpServletResponse responseConf){
		GenericResponse<Category> response = new GenericResponse<>();
		try {
			categoryDao.delete(category);
			response.setData(category);
			response.setMessage(MessageResponse.DELETED);
		} catch (Exception ex) {
			response.setMessage(MessageResponse.GENERIC_ERROR);
			responseConf.setStatus(500);
		}

		return response;
	}

}
