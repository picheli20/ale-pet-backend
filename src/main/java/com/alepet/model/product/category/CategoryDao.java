package com.alepet.model.product.category;

import javax.transaction.Transactional;

import org.springframework.data.repository.CrudRepository;

@Transactional
public interface CategoryDao  extends CrudRepository<Category, Long> {

}
