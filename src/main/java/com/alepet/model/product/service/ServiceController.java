package com.alepet.model.product.service;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.alepet.model.product.size.Size;
import com.alepet.model.product.size.SizeDao;
import com.alepet.util.GenericResponse;
import com.alepet.util.MessageResponse;
import com.alepet.util.Util;

@RestController
@RequestMapping("api/v1/service")
public class ServiceController {

	@Autowired
	private ServiceDao serviceDao;
	@Autowired
	private SizeDao sizeDao;

	@RequestMapping(path="/", method=RequestMethod.GET)
	public GenericResponse<Iterable<Service>> getService(){
		GenericResponse<Iterable<Service>> response = new GenericResponse<>();
		response.setData(serviceDao.findAll());
		response.setMessage(MessageResponse.OK);
		return response;
	}

	@RequestMapping(path="/", method=RequestMethod.PUT)
	public GenericResponse<Service> getService(@RequestBody Service service, HttpServletResponse responseConf){
		GenericResponse<Service> response = new GenericResponse<>();
		try {
			Iterable<Size> savedSizes = sizeDao.save(service.getSize());
			service.setSize(Util.IterableToList(savedSizes));
			response.setData(serviceDao.save(service));
			response.setMessage(MessageResponse.CREATED);
		} catch (Exception ex) {
			response.setMessage(MessageResponse.GENERIC_ERROR);
			responseConf.setStatus(500);
		}

		return response;
	}

	@RequestMapping(path="/", method=RequestMethod.POST)
	public GenericResponse<Service> postService(@RequestBody Service service, HttpServletResponse responseConf){
		GenericResponse<Service> response = new GenericResponse<>();
		try {
			List<Size> sizes = service.getSize();
			List<Size> savedSizes = new ArrayList<>();
			for (Size item : sizes) {
				savedSizes.add(sizeDao.save(item));
			}
			service.setSize(Util.IterableToList(savedSizes));
			response.setData(serviceDao.save(service));
			response.setMessage(MessageResponse.UPDATED);
		} catch (Exception ex) {	
			response.setMessage(MessageResponse.GENERIC_ERROR);
			responseConf.setStatus(500);
		}

		return response;
	}

	@RequestMapping(path="/{id}", method=RequestMethod.DELETE)
	public GenericResponse<Service> deleteService(@PathVariable Long id, HttpServletResponse responseConf){
		Service service = serviceDao.findById(id);
		GenericResponse<Service> response = new GenericResponse<>();
		try {
			serviceDao.delete(service);
			sizeDao.delete(service.getSize());
			response.setData(service);
			response.setMessage(MessageResponse.DELETED);
		} catch (Exception ex) {
			response.setMessage(MessageResponse.GENERIC_ERROR);
			responseConf.setStatus(500);
		}
		return response;
	}
}
