package com.alepet.model.product.service;

import org.springframework.data.repository.CrudRepository;

public interface ServiceDao extends CrudRepository<Service, Long>{
	public Service findByName(String name);
	public Service findById(Long id);
}
