package com.alepet.model.product.size;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.alepet.util.GenericResponse;
import com.alepet.util.MessageResponse;

@RestController
@RequestMapping("api/v1/size")
public class SizeController {
	
	@Autowired
	private SizeDao sizeDao;

	@RequestMapping(path="/", method=RequestMethod.GET)
	public GenericResponse<Iterable<Size>> getSize(){
		GenericResponse<Iterable<Size>> response = new GenericResponse<>();
		response.setData(sizeDao.findAll());
		response.setMessage(MessageResponse.OK);
		return response;
	}

	@RequestMapping(path="/", method=RequestMethod.PUT)
	public GenericResponse<Size> getSize(@RequestBody Size size, HttpServletResponse responseConf){
		GenericResponse<Size> response = new GenericResponse<>();
		try {
			response.setData(sizeDao.save(size));
			response.setMessage(MessageResponse.CREATED);
		} catch (Exception ex) {
			response.setMessage(MessageResponse.GENERIC_ERROR);
			responseConf.setStatus(500);
		}

		return response;
	}

	@RequestMapping(path="/", method=RequestMethod.POST)
	public GenericResponse<Size> postSize(@RequestBody Size size, HttpServletResponse responseConf){
		GenericResponse<Size> response = new GenericResponse<>();
		try {
			response.setData(sizeDao.save(size));
			response.setMessage(MessageResponse.UPDATED);
		} catch (Exception ex) {
			response.setMessage(MessageResponse.GENERIC_ERROR);
			responseConf.setStatus(500);
		}

		return response;
	}

	@RequestMapping(path="/", method=RequestMethod.DELETE)
	public GenericResponse<Size> deleteSize(@RequestBody Size size, HttpServletResponse responseConf){
		GenericResponse<Size> response = new GenericResponse<>();
		try {
			sizeDao.delete(size);
			response.setData(size);
			response.setMessage(MessageResponse.DELETED);
		} catch (Exception ex) {
			response.setMessage(MessageResponse.GENERIC_ERROR);
			responseConf.setStatus(500);
		}
		return response;
	}
}
