package com.alepet.model.product.size;

import org.springframework.data.repository.CrudRepository;

public interface SizeDao extends CrudRepository<Size, Long>{

}
